export class Catedratico {
    nombre: string;
    fechaNacimiento: Date;
    codigoIdentidad: number;
    tipoCatedratico: number;
    correoCatedratico: string;
    idInstitucionEducativa: number;

    constructor(nombre: string, fechaNacimiento: Date, codigoIdentidad: number, tipoCatedratico: number, correoCatedratico: string, idInstitucionEducativa: number) {
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
        this.codigoIdentidad = codigoIdentidad;
        this.tipoCatedratico = tipoCatedratico;
        this.correoCatedratico = correoCatedratico;
        this.idInstitucionEducativa = idInstitucionEducativa;
    }
}
