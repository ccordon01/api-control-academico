export class Usuario {
    idUsuario?: number;
    nombreUsuario: string;
    claveUsuario: string;
    fechaCreacionUsuario?: Date;
    fechaUltimaModificacionUsuario?: Date;
    

    constructor(nombreUsuario: string, claveUsuario: string) {
        this.nombreUsuario = nombreUsuario;
        this.claveUsuario = claveUsuario;
    }
}
