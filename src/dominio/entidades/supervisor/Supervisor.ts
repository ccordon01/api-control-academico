export class Supervisor {
    nombreSupervisor: string;
    fechaNacimientoSupervisor: Date;
    codigoIdentidadSupervisor: number;
    idTipoSupervisor: number;
    correoElectronico: string;
    telefono: number;


    constructor(nombreSupervisor: string, fechaNacimientoSupervisor: Date, codigoIdentidadSupervisor: number,idTipoSupervisor:number,correoElectronico: string,telefono: number) {
        this.nombreSupervisor = nombreSupervisor;
        this.fechaNacimientoSupervisor=fechaNacimientoSupervisor;
        this.codigoIdentidadSupervisor = codigoIdentidadSupervisor;
        this.idTipoSupervisor = idTipoSupervisor;
        this.correoElectronico = correoElectronico;
        this.telefono = telefono;
    }
}
