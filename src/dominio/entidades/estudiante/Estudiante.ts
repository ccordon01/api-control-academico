export class Estudiante {
    idEstudiante?: number;
    nombreEstudiante?: string;
    fechaNacimiento?: number;
    gradoActualEstudiante?: string;
    seccionActualEstudiante?: string;
    fechaCreacionEstudiante?: Date;
    fechaUltimaModificacionEstudiante?: Date;
    idUsuarioEstudiante?: number;

    constructor(idEstudiante: number, nombreEstudiante: string, fechaNacimiento: number, 
                gradoActualEstudiante: string, seccionActualEstudiante: string, fechaCreacionEstudiante: Date, 
                fechaUltimaModificacionEstudiante: Date, idUsuarioEstudiante: number) {
        this.idEstudiante = idEstudiante;
        this.nombreEstudiante = nombreEstudiante;
        this.fechaNacimiento = fechaNacimiento;
        this.gradoActualEstudiante = gradoActualEstudiante;
        this.seccionActualEstudiante = seccionActualEstudiante;
        this.fechaCreacionEstudiante = fechaCreacionEstudiante;
        this.fechaUltimaModificacionEstudiante = fechaUltimaModificacionEstudiante;
        this.idUsuarioEstudiante = idUsuarioEstudiante;
    }
}

