export class Colegio {
    idInstitucionEducativa?: number;
    nombreInstitucionEducativa?: string;
    direccionInstitucionEducativa?: number;
    contactoInstitucionEducativa?: string;
    idCatedraticoPrincipal?: string;
    fecraCreacionInstitucionEducativa?: Date;
    fechaUltimaMOdificacionInstitucionEducativa?: Date;

    constructor(idInstitucion: number, nombreInstitucion: string, direccionInstitucion: number, 
                contactoInstitucion: string, idCatedratico: string, fechaCreacion: Date, 
                fechaUltimaMod: Date) {
        this.idInstitucionEducativa = idInstitucion;
        this.nombreInstitucionEducativa = nombreInstitucion;
        this.direccionInstitucionEducativa = direccionInstitucion;
        this.contactoInstitucionEducativa = contactoInstitucion;
        this.idCatedraticoPrincipal = idCatedratico;
        this.fecraCreacionInstitucionEducativa = fechaCreacion;
        this.fechaUltimaMOdificacionInstitucionEducativa = fechaUltimaMod;
    }
}

