import dotenv from 'dotenv';
import cors from 'cors';
import express from 'express';

import { usuarioControlador } from './interfaz/controladores/usuarioControlador';
import { catedraticoControlador } from './interfaz/controladores/catedratico/catedraticoControlador';
import { institucionController } from './interfaz/controladores/institucion/institucionControlador';

dotenv.config();

const PORT: number = process.argv[2] ? parseInt(process.argv[2] as string) : parseInt(process.env.PORT as string);

const app = express();

app.use(cors());
app.use(express.json());
app.use('/usuario', usuarioControlador);
app.use('/institucioneducativa', institucionController);
app.use('/catedratico', catedraticoControlador);

app.listen(PORT, () => {
    console.log('Server funcionando!');
});
