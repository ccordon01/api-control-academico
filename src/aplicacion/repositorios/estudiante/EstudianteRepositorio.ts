import { Estudiante } from '../../../dominio/entidades/estudiante/Estudiante';

export interface EstudianteRepositorio {
    crearEstudiante(estudiante: Estudiante): Promise<void>;

    ListarEstudiante(): Promise<Estudiante[]>;
}