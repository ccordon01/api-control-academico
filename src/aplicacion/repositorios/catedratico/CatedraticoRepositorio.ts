import { Usuario } from '../../../dominio/entidades/Usuario';
import { Catedratico } from '../../../dominio/entidades/catedratico/Catedratico';

export interface CatedraticoRepositorio {
    crearCatedratico(catedratico: Catedratico, usuario: Usuario): Promise<void>;
    listarCatedraticos(): Promise<Catedratico[]>;

}