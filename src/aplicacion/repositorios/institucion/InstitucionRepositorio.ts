import { Colegio } from '../../../dominio/entidades/institucion/Colegio';

export interface InstitucionRepositorio {
    crearInstitucion(institucion: Colegio): Promise<void>;

    listarInstituciones(): Promise<Colegio[]>;
}