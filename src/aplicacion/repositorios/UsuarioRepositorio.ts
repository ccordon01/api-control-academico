import { Usuario } from '../../dominio/entidades/Usuario';

export interface UsuarioRepositorio {
    crearUsuario(usuario: Usuario): Promise<void>;

    listarUsuarios(): Promise<Usuario[]>;
}