import { Supervisor } from '../../../dominio/entidades/supervisor/Supervisor';

export interface SupervisorRepositorio {
    crearSupervisor(supervisor: Supervisor): Promise<void>;
    listarSupervisores(): Promise<Supervisor[]>;

}