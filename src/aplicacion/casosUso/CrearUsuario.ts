import { Usuario } from '../../dominio/entidades/Usuario';
import { UsuarioRepositorio } from '../repositorios/UsuarioRepositorio';

export class CrearUsuario {
    private usuarioRepositorio: UsuarioRepositorio;

    constructor(usuarioRepositorio: UsuarioRepositorio) {
        this.usuarioRepositorio = usuarioRepositorio;
    }

    ejecutar(usuario: Usuario): Promise<void> {
        return this.usuarioRepositorio.crearUsuario(usuario);
    } 
}