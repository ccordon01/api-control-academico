import { Estudiante } from '../../../dominio/entidades/estudiante/Estudiante';
import { EstudianteRepositorio } from '../../repositorios/estudiante/EstudianteRepositorio';

export class ListarEstudiantes {
    private estudianteRepositorio: EstudianteRepositorio;

    constructor(estudianteRepositorio: EstudianteRepositorio) {
        this.estudianteRepositorio = estudianteRepositorio;
    }

    ejecutar(): Promise<Estudiante[]> {
        return this.estudianteRepositorio.ListarEstudiante();
    } 
}