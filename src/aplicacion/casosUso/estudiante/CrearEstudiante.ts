import { Estudiante } from '../../../dominio/entidades/estudiante/Estudiante';
import { EstudianteRepositorio } from '../../repositorios/estudiante/EstudianteRepositorio';

export class CrearEstudiante {
    private estudianteRepositorio: EstudianteRepositorio;

    constructor(estudianteRepositorio: EstudianteRepositorio) {
        this.estudianteRepositorio = estudianteRepositorio;
    }

    ejecutar(usuario: Estudiante): Promise<void> {
        return this.estudianteRepositorio.crearEstudiante(usuario);
    } 
}