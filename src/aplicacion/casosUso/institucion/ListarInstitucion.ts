import { Colegio } from '../../../dominio/entidades/institucion/Colegio';
import { InstitucionRepositorio } from '../../repositorios/institucion/InstitucionRepositorio';

export class ListarInstitucion {
    private institucionRepositorio: InstitucionRepositorio;

    constructor(institucionRepositorio: InstitucionRepositorio) {
        this.institucionRepositorio = institucionRepositorio;
    }

    ejecutar(): Promise<Colegio[]> {
        return this.institucionRepositorio.listarInstituciones();
    } 
}