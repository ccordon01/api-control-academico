import { Colegio } from '../../../dominio/entidades/institucion/Colegio';
import { InstitucionRepositorio } from '../../repositorios/institucion/InstitucionRepositorio';

export class CrearInstitucion {
    private institucionRepositorio: InstitucionRepositorio;

    constructor(institucionRepositorio: InstitucionRepositorio) {
        this.institucionRepositorio = institucionRepositorio;
    }

    ejecutar(usuario: Colegio): Promise<void> {
        return this.institucionRepositorio.crearInstitucion(usuario);
    } 
}