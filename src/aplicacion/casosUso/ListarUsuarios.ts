import { Usuario } from '../../dominio/entidades/Usuario';
import { UsuarioRepositorio } from '../repositorios/UsuarioRepositorio';

export class ListarUsuarios {
    private usuarioRepositorio: UsuarioRepositorio;

    constructor(usuarioRepositorio: UsuarioRepositorio) {
        this.usuarioRepositorio = usuarioRepositorio;
    }

    ejecutar(): Promise<Usuario[]> {
        return this.usuarioRepositorio.listarUsuarios();
    } 
}