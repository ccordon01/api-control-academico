import { Supervisor } from '../../../dominio/entidades/supervisor/Supervisor';
import { SupervisorRepositorio } from '../../repositorios/supervisor/SupervisorRepositorio';

export class ListarSupervisores {
    private supervisorRepositorio: SupervisorRepositorio;

    constructor(supervisorRepositorio: SupervisorRepositorio) {
        this.supervisorRepositorio = supervisorRepositorio;
    }

    ejecutar(): Promise<Supervisor[]> {
        return this.supervisorRepositorio.listarSupervisores();
    } 
}