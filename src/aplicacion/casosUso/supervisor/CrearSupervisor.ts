import { Supervisor } from '../../../dominio/entidades/supervisor/Supervisor';
import { SupervisorRepositorio } from '../../repositorios/supervisor/SupervisorRepositorio';

export class CrearSupervisor {
    private supervisorRepositorio: SupervisorRepositorio;

    constructor(supervisorRepositorio: SupervisorRepositorio) {
        this.supervisorRepositorio = supervisorRepositorio;
    }

    ejecutar(supervisor: Supervisor): Promise<void> {
        return this.supervisorRepositorio.crearSupervisor(supervisor);
    } 
}