import { Catedratico } from '../../../dominio/entidades/catedratico/Catedratico';
import { CatedraticoRepositorio } from '../../repositorios/catedratico/CatedraticoRepositorio';

export class ListarCatedraticos {
    private catedraticoRepositorio: CatedraticoRepositorio;

    constructor(catedraticoRepositorio: CatedraticoRepositorio) {
        this.catedraticoRepositorio = catedraticoRepositorio;
    }

    ejecutar(): Promise<Catedratico[]> {
        return this.catedraticoRepositorio.listarCatedraticos();
    } 
}