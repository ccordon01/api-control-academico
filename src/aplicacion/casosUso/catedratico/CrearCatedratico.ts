import { Usuario } from '../../../dominio/entidades/Usuario';
import { Catedratico } from '../../../dominio/entidades/catedratico/Catedratico';
import { CatedraticoRepositorio } from '../../repositorios/catedratico/CatedraticoRepositorio';

export class CrearCatedratico {
    private catedraticoRepositorio: CatedraticoRepositorio;

    constructor(catedraticoRepositorio: CatedraticoRepositorio) {
        this.catedraticoRepositorio = catedraticoRepositorio;
    }

    ejecutar(catedratico: Catedratico, usuario: Usuario): Promise<void> {
        return this.catedraticoRepositorio.crearCatedratico(catedratico, usuario);
    } 
}