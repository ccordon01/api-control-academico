// Configuracion de la libreria
import { MensajeValidador } from '../MensajeValidador'
import Ajv from 'ajv';
const ajv: Ajv.Ajv = new Ajv({ allErrors: true });

const schema = {
    properties: {
        estudiante: {
            type: 'object',
            properties: {
                idEstudiante: { type: 'number' },
                nombreEstudiante: { type: 'string' },
                fechaNacimiento: { type: 'number' },
                gradoActualEstudiante: { type: 'string' },
                seccionActualEstudiante: { type: 'string' },
                fechaCreacionEstudiante: { type: 'Date' },
                fechaUltimaModificacionEstudiante: { type: 'Date' },
                idUsuarioEstudiante: { type: 'number' }
            },
            required: ['nombreEstudiante', 'fechaNacimiento'],
            additonalProperties: false
        }
    },
    required: ['estudiante'],
    additonalProperties: false
};

const validacion = ajv.compile(schema);

export function estudianteValidadorEntrada(estudiante: any): MensajeValidador {
    const estadoValidacion = validacion(estudiante);
    const mensajeValidacion = ajv.errorsText(validacion.errors);
    return {
        estado: estadoValidacion as boolean,
        mensaje: mensajeValidacion
    } as MensajeValidador;
}

