// Configuracion de la libreria
import { MensajeValidador } from '../MensajeValidador'
import Ajv from 'ajv';
const ajv: Ajv.Ajv = new Ajv({ allErrors: true });

const schema = {
    properties: {
        institucion: {
            type: 'object',
            properties: {
                idInstitucionEducativa:{ type:'number'},
                nombreInstitucionEducativa:{ type:'string'},
                direccionInstitucionEducativa:{ type:'string'},
                contactoInstitucionEducativa:{ type:'string'},
                idCatedraticoPrincipal:{ type:'string'},
                fecraCreacionInstitucionEducativa:{ type:'string'},
                fechaUltimaModificacionInstitucionEducativa:{ type:'string'}
            },
            required: ['nombreInstitucionEducativa', 'direccionInstitucionEducativa','contactoInstitucionEducativa'],
            additonalProperties: false
        }
    },
    required: ['institucion'],
    additonalProperties: false
};

const validacion = ajv.compile(schema);

export function institucionValidadorEntrada(institucion: any): MensajeValidador {
    const estadoValidacion = validacion(institucion);
    const mensajeValidacion = ajv.errorsText(validacion.errors);
    return {
        estado: estadoValidacion as boolean,
        mensaje: mensajeValidacion
    } as MensajeValidador;
}


