export interface MensajeValidador {
    estado: boolean;
    mensaje: string;
}