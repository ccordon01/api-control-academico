// Configuracion de la libreria
import { MensajeValidador } from '../MensajeValidador'
import Ajv from 'ajv';
import { Catedratico } from '../../../dominio/entidades/catedratico/Catedratico';
const ajv: Ajv.Ajv = new Ajv({ allErrors: true });

const schema = {
    properties: {
        catedratico: {
            type: 'object',
            properties: {
                nombre: { type: 'string' },
                fechaNacimiento: { type: 'string' },
                codigoIdentidad: { type: 'string', maxLength: 13, minLength: 13 },
                tipoCatedratico: {type: 'number'},
                correoCatedratico: {type: 'string'},
                idInstitucionEducativa: {type:'number'}
            },
            required: ['nombre', 'fechaNacimiento', 'codigoIdentidad','tipoCatedratico','correoCatedratico','idInstitucionEducativa'],
            additonalProperties: false
        }
    },
    required: ['catedratico'],
    additonalProperties: false
};


const validacion = ajv.compile(schema);

export function catedraticoValidadorEntrada(catedratico: any): MensajeValidador {
    const estadoValidacion = validacion(catedratico);
    const mensajeValidacion = ajv.errorsText(validacion.errors);
    return {
        estado: estadoValidacion as boolean,
        mensaje: mensajeValidacion
    } as MensajeValidador;
}