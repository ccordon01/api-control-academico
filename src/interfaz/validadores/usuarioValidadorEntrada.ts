// Configuracion de la libreria
import { MensajeValidador } from './MensajeValidador'
import Ajv from 'ajv';
const ajv: Ajv.Ajv = new Ajv({ allErrors: true });

const schema = {
    properties: {
        usuario: {
            type: 'object',
            properties: {
                nombreUsuario: { type: 'string' },
                claveUsuario: { type: 'string', pattern: "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d\\W]{8,63}$" },
            },
            required: ['nombreUsuario', 'claveUsuario'],
            additonalProperties: false
        }
    },
    required: ['usuario'],
    additonalProperties: false
};

const validacion = ajv.compile(schema);

export function usuarioValidadorEntrada(usuario: any): MensajeValidador {
    const estadoValidacion = validacion(usuario);
    const mensajeValidacion = ajv.errorsText(validacion.errors);
    return {
        estado: estadoValidacion as boolean,
        mensaje: mensajeValidacion
    } as MensajeValidador;
}