// Configuracion de la libreria
import { MensajeValidador } from '../MensajeValidador'
import Ajv from 'ajv';
import { Supervisor } from '../../../dominio/entidades/supervisor/Supervisor';
const ajv: Ajv.Ajv = new Ajv({ allErrors: true });

const schema = {
    properties: {
        supervisor: {
            type: 'object',
            properties: {
                nombre: { type: 'string' },
                fechaNacimiento: { type: 'string' },
                codigoIdentidad: { type: 'number', maximum: 13 },
                idTipoSupervisor: {type: 'number'},
                correoElectronico: {type: 'string'},
                telefono: {type: 'number'}
            },
            required: ['nombre', 'fechaNacimiento', 'codigoIdentidad','idTipoSupervisor','correoElectronico','telefono'],
            additonalProperties: false
        }
    },
    required: ['supervisor'],
    additonalProperties: false
};


const validacion = ajv.compile(schema);

export function supervisorValidadorEntrada(supervisor: any): MensajeValidador {
    const estadoValidacion = validacion(supervisor);
    const mensajeValidacion = ajv.errorsText(validacion.errors);
    return {
        estado: estadoValidacion as boolean,
        mensaje: mensajeValidacion
    } as MensajeValidador;
}