import express, { Request, Response } from 'express';
import { Usuario } from '../../dominio/entidades/Usuario';
import { CrearUsuario } from '../../aplicacion/casosUso/CrearUsuario';
import { ListarUsuarios } from '../../aplicacion/casosUso/ListarUsuarios';
import { UsuarioRepositorioMemoria } from '../../infrastructura/almacenamiento/UsuarioRepositorioMemoria';
import { UsuarioRepositorioService } from '../../infrastructura/almacenamiento/UsuarioRepositorioService';
import { MensajeValidador } from '../validadores/MensajeValidador';
import { usuarioValidadorEntrada } from '../validadores/usuarioValidadorEntrada';

const usuarioRepositorioMemoria: UsuarioRepositorioMemoria = new UsuarioRepositorioMemoria();
const usuarioRepositorioService: UsuarioRepositorioService = new UsuarioRepositorioService();
const crearUsuario: CrearUsuario = new CrearUsuario(usuarioRepositorioService);
const listarUsuarios: ListarUsuarios = new ListarUsuarios(usuarioRepositorioMemoria);

export const usuarioControlador = express.Router();

usuarioControlador.get('/todos', async (req: Request, res: Response) => {
    try {
        const usuarios: Usuario[] = await listarUsuarios.ejecutar();
        res.status(200).send(usuarios);
    } catch (error) {
        res.status(400).send();
    }
});

usuarioControlador.post('/crear', async (req: Request, res: Response) => {
    try {
        const validacion: MensajeValidador = usuarioValidadorEntrada(req.body);
        if (validacion.estado) {
            const usuario: Usuario = req.body.usuario;
            await crearUsuario.ejecutar(usuario);
            return res.status(200).send('Usuario creado.');
        }
        res.status(400).send({ mensaje: validacion.mensaje });
    } catch (error) {
        res.status(400).send();
    }
});