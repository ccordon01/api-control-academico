import express, { Request, Response } from 'express';
import { Estudiante } from '../../../dominio/entidades/estudiante/Estudiante';
import { CrearEstudiante } from '../../../aplicacion/casosUso/estudiante/CrearEstudiante';
import { ListarEstudiantes } from '../../../aplicacion/casosUso/estudiante/ListarEstudiante';
import { EstudianteRepositorioMemoria } from '../../../infrastructura/almacenamiento/EstudianteRepositorioMemoria';
import { MensajeValidador } from '../../validadores/MensajeValidador';
import { estudianteValidadorEntrada } from '../../validadores/estudiante/estudianteValidadorEntrada';

const estudianteRepositorioMemoria: EstudianteRepositorioMemoria = new EstudianteRepositorioMemoria();
const crearEstudiante: CrearEstudiante = new CrearEstudiante(estudianteRepositorioMemoria);
const listarEstudiante: ListarEstudiantes = new ListarEstudiantes(estudianteRepositorioMemoria);

export const estudianteControlador = express.Router();

estudianteControlador.get('/todos', async (req: Request, res: Response) => {
    try {
        const estudiante: Estudiante[] = await listarEstudiante.ejecutar();
        res.status(200).send(estudiante);
    } catch (error) {
        res.status(400).send();
    }
});

estudianteControlador.post('/crear', async (req: Request, res: Response) => {
    try {
        const validacion: MensajeValidador = estudianteValidadorEntrada(req.body);
        if (validacion.estado) {
            const estudiante: Estudiante = req.body.estudiante;
            await crearEstudiante.ejecutar(estudiante);
            return res.status(200).send('Estudiante creado.');
        }
        res.status(400).send({ mensaje: validacion.mensaje });
    } catch (error) {
        res.status(400).send();
    }
});