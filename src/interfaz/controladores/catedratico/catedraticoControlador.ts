import express, { Request, Response } from 'express';
import { Usuario } from '../../../dominio/entidades/Usuario';
import { Catedratico } from '../../../dominio/entidades/catedratico/Catedratico';
import { CrearCatedratico } from '../../../aplicacion/casosUso/catedratico/CrearCatedratico';
import { CatedraticoRepositorioMemoria } from '../../../infrastructura/almacenamiento/CatedraticoRepositorioMemoria';
import { CatedraticoRepositorioService } from '../../../infrastructura/almacenamiento/CatedraticoRepositorioService';
import { ListarCatedraticos } from '../../../aplicacion/casosUso/catedratico/ListarCatedraticos';
import { MensajeValidador } from '../../validadores/MensajeValidador';
import { catedraticoValidadorEntrada } from '../../validadores/catedratico/catedraticoValidadorEntrada';
import { usuarioValidadorEntrada } from '../../validadores/usuarioValidadorEntrada';

const catedraticoRepositorioMemoria: CatedraticoRepositorioMemoria = new CatedraticoRepositorioMemoria();
const catedraticoRepositorioService: CatedraticoRepositorioService = new CatedraticoRepositorioService();
const crearCatedratico: CrearCatedratico = new CrearCatedratico(catedraticoRepositorioService);
const listarCatedraticos: ListarCatedraticos = new ListarCatedraticos(catedraticoRepositorioService);

export const catedraticoControlador = express.Router();

catedraticoControlador.post('/crear', async (req: Request, res: Response) => {
    try {
        const validacionCatedratico: MensajeValidador
            = catedraticoValidadorEntrada(req.body.catedratico && { catedratico: req.body.catedratico });
        const validacionUsuario: MensajeValidador
            = usuarioValidadorEntrada(req.body.usuario && { usuario: req.body.usuario });
        if (validacionCatedratico.estado && validacionUsuario.estado) {
            const usuario: Usuario = req.body.usuario;
            const catedratico: Catedratico = req.body.catedratico;
            await crearCatedratico.ejecutar(catedratico, usuario);
            return res.status(200).send('Catedratico creado.');
        }
        console.log(JSON.stringify(req.body))
        console.log(validacionCatedratico.mensaje)
        console.log(validacionUsuario.mensaje)
        res.status(400).send("ok");
    } catch (error) {
        res.status(400).send();
    }
});

catedraticoControlador.get('/todos', async (req: Request, res: Response) => {
    try {
        const catedraticos: Catedratico[] = await listarCatedraticos.ejecutar();
        res.status(200).send(catedraticos);
    } catch (error) {
        res.status(400).send();
    }
});