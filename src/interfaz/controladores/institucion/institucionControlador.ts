import express, { Request, Response } from 'express';
import { Colegio } from '../../../dominio/entidades/institucion/Colegio';
import { CrearInstitucion } from '../../../aplicacion/casosUso/institucion/CrearInstitucion';
import { ListarInstitucion } from '../../../aplicacion/casosUso/institucion/ListarInstitucion';
import { InstitucionRepositorioMemoria } from '../../../infrastructura/almacenamiento/InstitucionRepositorioMemoria';
import { InstitucionRepositorioService } from '../../../infrastructura/almacenamiento/InstitucionRepositorioService';
import { MensajeValidador } from '../../validadores/MensajeValidador';
import { institucionValidadorEntrada } from '../../validadores/institucion/institucionValidadorEntrada';

const institucionRepositorioMemoria: InstitucionRepositorioMemoria = new InstitucionRepositorioMemoria();
const institucionRepositorioService: InstitucionRepositorioService = new InstitucionRepositorioService();
const crearInstitucion: CrearInstitucion = new CrearInstitucion(institucionRepositorioService);
const listarInstitucion: ListarInstitucion = new ListarInstitucion(institucionRepositorioService);

export const institucionController = express.Router();

institucionController.get('/todos', async (req: Request, res: Response) => {
    try {
        const colegio: Colegio[] = await listarInstitucion.ejecutar();
        res.status(200).send(colegio);
    } catch (error) {
        res.status(400).send();
    }
});

institucionController.post('/crear', async (req: Request, res: Response) => {
    try {
        const validacion: MensajeValidador = institucionValidadorEntrada(req.body);
        if (validacion.estado) {
            const institucion: Colegio = req.body.institucion;
            await crearInstitucion.ejecutar(institucion);
            return res.status(200).send('Institucion creada.');
        }
        res.status(400).send({ mensaje: validacion.mensaje });
    } catch (error) {
        res.status(400).send();
    }
});