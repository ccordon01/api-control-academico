import express, { Request, Response } from 'express';
import { Supervisor } from '../../../dominio/entidades/supervisor/Supervisor';
import { CrearSupervisor } from '../../../aplicacion/casosUso/supervisor/CrearSupervisor';
import { SupervisorRepositorioMemoria } from '../../../infrastructura/almacenamiento/SupervisorRepositorioMemoria';
import { ListarSupervisores } from '../../../aplicacion/casosUso/supervisor/ListarSupervisores';
import { MensajeValidador } from '../../validadores/MensajeValidador';
import { supervisorValidadorEntrada } from '../../validadores/supervisor/supervisorValidadorEntrada';

const supervisorRepositorioMemoria: SupervisorRepositorioMemoria = new SupervisorRepositorioMemoria();
const crearSupervisor: CrearSupervisor = new CrearSupervisor(supervisorRepositorioMemoria);
const listarSupervisores: ListarSupervisores = new ListarSupervisores(supervisorRepositorioMemoria);

export const supervisorControlador = express.Router();

supervisorControlador.post('/crear', async (req: Request, res: Response) => {
    try {
        const validacion: MensajeValidador = supervisorValidadorEntrada(req.body);
        if (validacion.estado) {
        const supervisor: Supervisor = req.body.supervisor;
        await crearSupervisor.ejecutar(supervisor);
        return res.status(200).send('Supervisor creado.');
        }
        res.status(400).send("ok");
    } catch (error) {
        res.status(400).send();
    }
});

supervisorControlador.get('/todos', async (req: Request, res: Response) => {
    try {
        const supervisores: Supervisor[] = await listarSupervisores.ejecutar();
        res.status(200).send(supervisores);
    } catch (error) {
        res.status(400).send();
    }
});