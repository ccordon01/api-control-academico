import dotenv from 'dotenv';
dotenv.config();

import WebSocket from 'ws';
import { IMensajeWebSocket } from './IMensajeWebSocket';

export class WebSocketService {
    private static instance: WebSocketService;
    private webSocket: WebSocket | null;
    private connectionString: string;

    constructor(connectionString: string) {
        this.webSocket = null;
        this.connectionString = connectionString;
    }

    public static async getInstance(connectionString: string): Promise<WebSocketService> {
        if (!WebSocketService.instance) {
            WebSocketService.instance = new WebSocketService(connectionString);
            await this.conectarWebSocket();
        }
        return WebSocketService.instance;
    }

    public conectarWebSocket(): Promise<void> {
        if (this.webSocket) return Promise.resolve();
        return new Promise((resolve, reject) => {
            try {
                console.log('conectando');
                this.webSocket = new WebSocket(this.connectionString);
                this.webSocket.on('open', () => {
                    // Conectado!
                    console.log('conectado');
                    return resolve();
                });
            } catch (error) {
                return reject();
            }
        });
    }

    public enviarMensaje(data: IMensajeWebSocket): Promise<void> {
        return Promise.resolve(this.webSocket?.send(JSON.stringify(data)));
    }

    public recibirMensaje(): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                this.webSocket?.on('message', (data) => {
                    try {
                        return resolve(JSON.parse(data.toString()));
                    } catch (error) {
                        return resolve();
                    }
                });
            } catch (error) {
                return reject();
            }
        });
    }
}