export interface IMensajeWebSocket {
    entidad: string;
    accion: string;
    data: any;
}