import { MensajeValidador } from '../../interfaz/validadores/MensajeValidador';
import { usuarioValidadorEntrada } from '../../interfaz/validadores/usuarioValidadorEntrada';

describe('Formato Colegio', () => {
    it('Colegio valido para el sistema.', () => {
        const entrada = {
            "usuario": {
                "nombreUsuario": "user1",
                "claveUsuario": "Asdf4560$",
            }
        };
        const mensajeValidador: MensajeValidador = usuarioValidadorEntrada(entrada);
        console.log(mensajeValidador.mensaje)
        expect(mensajeValidador.estado).toBe(true)
    });

    it('Colegio no valido para el sistema.', () => {
        const entrada = {
            "usuario": {
                "nombreUsuario": 10,
                "claveUsuario": "Asdf4560$",
            }
        };
        const mensajeValidador: MensajeValidador = usuarioValidadorEntrada(entrada);
        expect(mensajeValidador.estado).toBe(false)
    });

    it('Colegio con password no valida para el sistema.', () => {
        const entrada = {
            "usuario": {
                "nombreUsuario": 10,
                "claveUsuario": "sdf4560",
            }
        };
        const mensajeValidador: MensajeValidador = usuarioValidadorEntrada(entrada);
        expect(mensajeValidador.estado).toBe(false)
    });

    it('Colegio sin parametros obligatorios.', () => {
        const entrada = {
            "usuario": {
                "nombreUsuario": "user1",
                "edad": 10
            }
        };
        const mensajeValidador: MensajeValidador = usuarioValidadorEntrada(entrada);
        expect(mensajeValidador.estado).toBe(false)
    });
});