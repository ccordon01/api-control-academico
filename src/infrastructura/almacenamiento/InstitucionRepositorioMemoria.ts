import { Colegio } from '../../dominio/entidades/institucion/Colegio';
import { InstitucionRepositorio } from '../../aplicacion/repositorios/institucion/InstitucionRepositorio';

export class InstitucionRepositorioMemoria implements InstitucionRepositorio {

    private data: Colegio[];

    constructor() {
        this.data = [];
    }

    crearInstitucion(institucion: Colegio): Promise<void> {
        this.data.push(institucion);
        return Promise.resolve();
    }

    listarInstituciones(): Promise<Colegio[]> {
        return Promise.resolve(this.data);
    }

}