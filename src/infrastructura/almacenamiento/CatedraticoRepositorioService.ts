import { IMensajeWebSocket } from '../externo/webSocket/IMensajeWebSocket';
import { WebSocketService } from '../externo/webSocket/WebSocketService'
import { Usuario } from '../../dominio/entidades/Usuario';
import { Catedratico } from '../../dominio/entidades/catedratico/Catedratico';
import { CatedraticoRepositorio } from '../../aplicacion/repositorios/catedratico/CatedraticoRepositorio';

export class CatedraticoRepositorioService implements CatedraticoRepositorio {

    constructor() {
    }

    async crearCatedratico(catedratico: Catedratico, usuario: Usuario): Promise<void> {
        try {
            const ws = await WebSocketService.getInstance();
            const data: IMensajeWebSocket = {
                entidad: 'Catedratico',
                accion: 'crear',
                data: { catedratico, usuario}
            };
            await ws.enviarMensaje(data);
            const datas = await ws.recibirMensaje();
            console.log(datas)
        } catch (error) {
            return Promise.reject();
        }
    }

    async listarCatedraticos(): Promise<Catedratico[]> {
        try {
            const ws = await WebSocketService.getInstance();
            const data: IMensajeWebSocket = {
                entidad: 'Catedratico',
                accion: 'obtener',
                data: null
            };
            await ws.enviarMensaje(data);
            const result: Catedratico[] = await ws.recibirMensaje();
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject();
        }
    }
}