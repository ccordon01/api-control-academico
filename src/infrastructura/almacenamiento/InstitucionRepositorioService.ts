import { IMensajeWebSocket } from '../externo/webSocket/IMensajeWebSocket';
import { WebSocketService } from '../externo/webSocket/WebSocketService'
import { Colegio } from '../../dominio/entidades/institucion/Colegio';
import { InstitucionRepositorio } from '../../aplicacion/repositorios/institucion/InstitucionRepositorio';

export class InstitucionRepositorioService implements InstitucionRepositorio {

    constructor() {
    }

    async crearInstitucion(institucion: Colegio): Promise<void> {
        try {
            const ws = await WebSocketService.getInstance();
            const data: IMensajeWebSocket = {
                entidad: 'Colegio',
                accion: 'crear',
                data: institucion
            };
            await ws.enviarMensaje(data);
            const datas = await ws.recibirMensaje();
            console.log(datas)
        } catch (error) {
            return Promise.reject();
        }
    }

    async listarInstituciones(): Promise<Colegio[]> {
        try {
            const ws = await WebSocketService.getInstance();
            const data: IMensajeWebSocket = {
                entidad: 'Colegio',
                accion: 'obtener',
                data: null
            };
            await ws.enviarMensaje(data);
            const result: Colegio[] = await ws.recibirMensaje();
            return Promise.resolve(result);
        } catch (error) {
            return Promise.reject();
        }
    }

}