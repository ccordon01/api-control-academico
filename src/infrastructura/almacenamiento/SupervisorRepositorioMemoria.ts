import { Supervisor } from '../../dominio/entidades/supervisor/Supervisor';
import { SupervisorRepositorio } from '../../aplicacion/repositorios/supervisor/SupervisorRepositorio';

export class SupervisorRepositorioMemoria implements SupervisorRepositorio {

    private data: Supervisor[];

    constructor() {
        this.data = [];
    }

    crearSupervisor(supervisor: Supervisor): Promise<void> {
        this.data.push(supervisor);
        return Promise.resolve();
    }

    listarSupervisores(): Promise<Supervisor[]> {
        return Promise.resolve(this.data);
    }
}