import { Usuario } from '../../dominio/entidades/Usuario';
import { UsuarioRepositorio } from '../../aplicacion/repositorios/UsuarioRepositorio';

export class UsuarioRepositorioMemoria implements UsuarioRepositorio {

    private data: Usuario[];
    private id: number;

    constructor() {
        this.data = [];
        this.id = 0;
    }

    crearUsuario(usuario: Usuario): Promise<void> {
        this.id++;
        usuario.idUsuario = this.id;
        usuario.fechaCreacionUsuario =  new Date();
        usuario.fechaUltimaModificacionUsuario =  new Date();
        this.data.push(usuario);
        return Promise.resolve();
    }

    listarUsuarios(): Promise<Usuario[]> {
        return Promise.resolve(this.data);
    }

}