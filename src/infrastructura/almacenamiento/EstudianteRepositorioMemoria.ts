import { Estudiante } from '../../dominio/entidades/estudiante/Estudiante';
import { EstudianteRepositorio } from '../../aplicacion/repositorios/estudiante/EstudianteRepositorio';

export class EstudianteRepositorioMemoria implements EstudianteRepositorio {

    private data: Estudiante[];

    constructor() {
        this.data = [];
    }

    crearEstudiante(estudiante: Estudiante): Promise<void> {
        this.data.push(estudiante);
        return Promise.resolve();
    }

    ListarEstudiante(): Promise<Estudiante[]> {
        return Promise.resolve(this.data);
    }

}