import { Catedratico } from '../../dominio/entidades/catedratico/Catedratico';
import { CatedraticoRepositorio } from '../../aplicacion/repositorios/catedratico/CatedraticoRepositorio';

export class CatedraticoRepositorioMemoria implements CatedraticoRepositorio {

    private data: Catedratico[];

    constructor() {
        this.data = [];
    }

    crearCatedratico(catedratico: Catedratico): Promise<void> {
        this.data.push(catedratico);
        return Promise.resolve();
    }

    listarCatedraticos(): Promise<Catedratico[]> {
        return Promise.resolve(this.data);
    }
}