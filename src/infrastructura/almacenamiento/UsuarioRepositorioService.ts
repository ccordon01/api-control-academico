import WebSocket from 'ws';
import { IMensajeWebSocket } from '../externo/webSocket/IMensajeWebSocket';
import { Usuario } from '../../dominio/entidades/Usuario';
import { UsuarioRepositorio } from '../../aplicacion/repositorios/UsuarioRepositorio';

export class UsuarioRepositorioService implements UsuarioRepositorio {
    private webSocket: WebSocket | null;

    constructor() {
        this.webSocket = null;
    }

    private conectarWebSocket(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                console.log('conectando');
                this.webSocket = new WebSocket('ws://localhost:8080');
                this.webSocket.on('open', () => {
                    // Conectado!
                    console.log('conectado');
                    return resolve();
                });
            } catch (error) {
                return reject();
            }
        });
    }

    private enviarMensaje(data: IMensajeWebSocket): Promise<void> {
        return Promise.resolve(this.webSocket?.send(JSON.stringify(data)));
    }

    private calcular(n: number): Promise<number> {
        return Promise.resolve(n);
    }

    async crearUsuario(usuario: Usuario): Promise<void> {
        try {
            await this.conectarWebSocket();
            const data: IMensajeWebSocket = {
                entidad: 'Usuario',
                accion: 'crear',
                data: usuario
            };
            await this.enviarMensaje(data);
        } catch (error) {
            return Promise.reject();
        }
    }

    listarUsuarios(): Promise<Usuario[]> {
        throw new Error("Method not implemented.");
    }

}